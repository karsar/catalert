CatAlert Project:

If you have a cat at home, it's sometimes not an easy task to find a safe place for your devices. I know that, as I have two of those lovely creatures leaving with me. You may put your Raspberry Pi in some safe place or just try CatAlert.

CatAlert quietly monitors the area via camera attached to the Raspberry Pi and whenever there is a cat approaching to your Pi (which serves as a media center, home cloud, webserver...), the software recognizes the cat being the cat, makes a photo of it and runs some audio file with a message for the cat.

Something as "Keeeeeepp youuuuurself far from my pie!!!!!!!!!! AAAAAAAAAAAAAAAAAA"

It takes another photo of the surprised cat for the owner's collection of funny photos
and waits for the another chance. Putting jokes aside, you may monitor how often your pet eats or scratches, or does anything else in a particular area. Moreover, you may point camera to some spot, which is dangerous for children and automatically remind them in case they forget and approach. 

Sounds easy and simple. Not so much. Software must include computer vision module, machine learning module to learn how cats or anything other look like, and be done in a low level coding style, as it has to run in parallel with the webserver or other important things, for which you got your Pi. 

Finally, this is not surveillance software, it doesn't save video, and it doesn't react on anything except defined target. My first step is to build "none efficient" Python prototype to see how limited will be the performance on Raspberry Pi.
 

